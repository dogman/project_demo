# app.py - a minimal flask api using flask_restful
from flask import Flask, render_template, redirect, url_for, request, make_response
import os
import io
import csv 
import pandas as pd
from pymongo import MongoClient
import json


import numpy as np

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV

from sklearn import metrics
from sklearn.metrics import accuracy_score, mean_squared_error, precision_recall_curve
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn import preprocessing
import joblib 




app = Flask(__name__)


# connection to docker mongo
client = MongoClient(
    os.environ['DB_PORT_27017_TCP_ADDR'],
    27017)

# connection to local mongo
#client = MongoClient('localhost', 27017)
db = client.customer_info_db


# landing page for full customer info
@app.route('/')
def full_customer_info():
    content = False
    return render_template('index.html', **locals())


# landing page for single customer info
@app.route('/single_customer_info')
def single_customer_info():
    summary_message = False

    return render_template('single_customer_page.html', summary_message=summary_message)



# 4. API endpoint to handle single customer info
@app.route('/handle_single_customer_info', methods=['POST'])
def handle_single_customer_info():
	single_customer_data = {}

	for item in [ 'age', 'job','marital', 'education', 'default', \
					'balance', 'housing', 'loan', 'contact', 'day', \
	    			'month', 'duration', 'campaign', 'pdays', 'previous', \
					'poutcome']:
		if item in ['age', 'balance', 'day', 'duration', 'campaign','pdays','previous']:
			single_customer_data[item] = int(request.form.get(item))
		else:
			single_customer_data[item] = request.form.get(item)

	print(single_customer_data)

	single_customer_data.update({'y': 0})

	# encode single customer data
	with open('tmp/label_dict.json', 'r') as f:
		label_dict = json.load(f)

	print(label_dict)
	for feature_name in ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'poutcome']:
	    customer_feature_value = single_customer_data[feature_name]
	    encoded_value = label_dict['label_' + feature_name].index(customer_feature_value) 
	    single_customer_data[feature_name] = encoded_value

	scd_df = pd.DataFrame([single_customer_data])
	print(scd_df)

	# normalize data
	scaler = joblib.load('tmp/scaler.pkl') 
	scd_df_scaled = scaler.transform(scd_df.values)

	print(scd_df_scaled)
	print(scd_df_scaled[:,:16])

	rf_model = joblib.load('tmp/rf_model.pkl') 
	y_pred_prob = rf_model.predict_proba(scd_df_scaled[:,:16])
	print('y pred prob', y_pred_prob)

	summary_message = 'Probability of the customer subscribing to the banking product: {}%'.format(y_pred_prob[0,1]*100)

	return render_template('single_customer_page.html', summary_message=summary_message)


# random forest model
def _random_forest(features_train, features_test, y_train, y_test):

    clf = RandomForestClassifier(n_estimators = 20)
    
    clf.fit(features_train, y_train)
    
    
    y_pred = clf.predict(features_test)
    
    y_pred_prob = clf.predict_proba(features_test)

    accuracy_score = metrics.accuracy_score(y_test, y_pred)

    print('accuracy: {} %'.format(accuracy_score * 100))
    
    print(type(y_pred_prob))
    print(y_pred_prob.shape)
    print(y_pred_prob[:2,:])
    print(y_pred_prob[:20,1]) # prob of y being yes
    
    return clf, accuracy_score

# 1. API endpoint to handle CSV file (full customer info)
@app.route('/handle_full_customer_info', methods=["POST"])
def handle_full_customer_info():
    #1. load CSV 
    f = request.files['data_file']
    if not f:
        return "No file"

    stream = io.StringIO(f.stream.read().decode("UTF8"), newline=None)
    csv_input = csv.reader(stream)

    
    # Pre-processing
    two_dim_list = []
    ctr=0
    for row in csv_input:
    	item_list = row[0].replace('"', '').split(';')
    	if ctr==0:
    		# first line is column names
    		col_names = item_list
    		print(col_names)
    	else:
    		two_dim_list.append(item_list)
    		item_dict = dict(zip(col_names, item_list))
    		print(item_dict)

    		# 2. load to db
    		db.customer_data.save(item_dict)

    	ctr+=1


    # 3. Built, train and validate ML Model
    # convert two dim list to dataframe
    customer_df=pd.DataFrame(two_dim_list,columns=col_names)
    print(customer_df.shape)
    print(customer_df.head())

    train_df = customer_df.copy()

    # encoding data
    label_dict = {}
    for feature_name in train_df:
    	if feature_name in ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'poutcome', 'y']:
    		# encode non-numerical features to numerical
    		le = preprocessing.LabelEncoder()
    		le.fit(train_df[feature_name])

    		train_df[feature_name] = le.transform(train_df[feature_name])

    		le_name_mapping = dict(zip(le.classes_, le.transform(le.classes_)))

    		# store mappings to dictionary
    		labelKey = 'label_' + feature_name
    		labelValue = [*le_name_mapping]
    		label_dict[labelKey] =labelValue

    # normalize data
    scaler = preprocessing.MinMaxScaler()
    train_df_scaled = scaler.fit_transform(train_df.values)

	# split data
    feature_data = train_df_scaled[:,:16] # first 16 columns are features
    y = train_df_scaled[:,16] # last 17th column is target variable
    features_train, features_test, y_train, y_test = train_test_split(feature_data, y, test_size=0.30, random_state=0)

	# train model, get accuracy score for testing set
    rf_model, accuracy_score = _random_forest(features_train, features_test, y_train, y_test)

    # save mapping dictionary
    json_data = json.dumps(label_dict)
    f = open("tmp/label_dict.json","w")
    f.write(json_data)
    f.close()

    # save scaler object
    joblib.dump(scaler, 'tmp/scaler.pkl') 

    # save ML model
    joblib.dump(rf_model, 'tmp/rf_model.pkl') 

    content = True
    accuracy_info = 'Model training is complete. The accuracy for testing set is: {0:.2f} %'.format(accuracy_score * 100)
    notification_text = '''
    	Mapping dictionary, scaler object, model have been saved and can be used to predict the probability 
    	of a new customer subscribing to the banking product. 
    	'''
    return render_template('index.html', **locals())



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
