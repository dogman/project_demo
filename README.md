
# Project Demo


### Description

Flask, MongoDB, Docker, Random Forest have been used for this project. 

1. In the script app.py, full_customer_info(), handle_full_customer_info() are used to handle the CSV file with full customer data. 

2. Once CSV file is uploaded to Flask, the data in CSV are stored to Mongo Database. 

3. The 70% of the full customer data are used to train the Random Forest model, rest 30% of the customer data are used to evaluate the model and generate accuracy score. Approximately 90% accuracy is achieved. 

4. The categorical variables in the customer data are encoded to numerical variables and all variables are properly normalized. Mapping dictionary for encoding and scaler object for normalization are saved so as to apply for new single customer data later. The trained Random Forest model is saved as well for prediction of new customer data. 

5. After full customer data are uploaded and Random Forest model is trained, single_customer_info(), handle_single_customer_info() are then used to accept new single customer data, pre-processing it and make prediction. 

6. Docker images are used to deploy Flask web framework and mongo database. 

### Installing and running the project

1. Clone this repo to local environment. 

2. Running following commands to deploy flask and mongo db to two seperate containers:

	```
	sudo docker-compose build
	```
	
	```
	sudo docker-compose up
	```

3. Go to 'full customer info' page via http://127.0.0.1:5000/

4. Upload CSV file, press submit button, the data in CSV will be stored into mongo database, and then the data are converted to pandas dataframe and fed into Random Forest model for training and testing. 

5. The 'full customer info' page will show the accuracy score of the model and notify the user that related mapping, scaling and model objects have been saved for later use. 

6. Once the Random Forest model is generated and saved, go to 'single customer info' page via http://127.0.0.1:5000/single_customer_info

7. Input information for the single customer, press submit button, then the data are sent to backend and used by the saved Random Forest model to predict how likely the customer will subscribe the banking product. 

8. Prediction result will display in the 'single customer info' page. 
9. The whole process is complete. 


## Authors

**Lei Xu**