FROM ubuntu:18.04

# Installation:
RUN apt-get update && apt-get install -y build-essential python3.6 
RUN apt-get install -y python3-setuptools
RUN apt-get install -y python3-pip
RUN apt-get install -y vim


COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt

